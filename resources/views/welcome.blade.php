<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
        <link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
        <!-- Styles -->
        <style>
        </style>
    </head>

    <body>

        <div id ="app" class="">
            <form class="update-form" v-on:submit.prevent>
                <input v-model="title" placeholder="Title" />
                <input v-model="body" placeholder="Body" />
                <button v-on:click="updateData()">Update</button>
                <button v-on:click="createData()">Create</button>
            </form>
            <p v-if="errors.length">
                <b>Please correct the following error(s):</b>
                <ul>
                <li v-for="error in errors">@{{error}}</li>
                </ul>
            </p>
            <table class="table table-hover" id="#myTable">
                <thead>
                    <tr>
                    <th scope="col">#</th>
                    <th scope="col">Title</th>
                    <th scope="col">Body</th>
                    <th scope="col">Update</th>
                    <th scope="col">Delete</th>
                    </tr>
                </thead>
                <tbody>
                    <tr v-for= "weatherData in weatherDataList" :key="weatherData.id">
                    <th scope="row">@{{weatherData.id}}</th>
                    <!-- <td><input type="text" :value="weatherData.title"></td> -->
                    <td>@{{weatherData.title}}</td>
                    <td>@{{weatherData.body}}</td>
                    <td><button v-on:click="updateButton(weatherData.title, weatherData.body)">Update</button></td>
                    <td><button v-on:click="deleteData(weatherData.title)">Delete</button></td>
                    </tr>
                </tbody>
            </table>
           
            
            <button v-on:click="readData()">Get Weather Data</button>
        </div>
    </body>
</html>
<!-- <script
  src="https://code.jquery.com/jquery-3.4.1.min.js"
  integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
  crossorigin="anonymous"></script> -->
<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>
<script src="https://unpkg.com/vue-cookies@1.5.12/vue-cookies.js"></script>
<!-- <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script> -->
<script>
    // $(document).ready( function () {
    //     $('#myTable').DataTable();
    // } );

    yourConfig = {
    }

    var makemethod = function(m_url,m_type,m_data){
        axios({
            method: m_type,
            url: m_url,
            if(m_data){
                data: m_data;
            },
            headers: {
                Authorization: "Bearer " + yourJWTToken
            }
        })
        .then(function(response){
            console.log(response);
            if(m_type=="post"){
                //save as cookie
            }
        })
        .catch(function(response){
            if(response.status==401){
                makemethod(refresh,'post');
            }
            if(m_type=="post"){
                //logout
            }
        });
    }
    
    // var postmethod = function(m_url,m_type,m_data){
    //     axios({
    //         method: m_type,
    //         url: m_url,
    //         data: m_data
    //     })
    //     .then(function(response){
    //         // Save as cookie
    //     })
    //     .catch(function(response){
    //         // logout
    //     });
    // }

    var app = new Vue({
    el: '#app',
    data: {
        message: 'Hello Vue! This is Cheny',
        weatherDataList: [],
        title: null,
        body: null,
        temp_title: null,
        errors: []
    },
    created() {
        var user = { id:1, name:'Cookie',session:'25j_7Sl6xDq2Kc3ym0fmrSSk2xV2XkUkX11'};
        var user2 = { id:2, name:'Cookie2',session:'25j_7Sl6xDq2Kc3ym0fmrSSk2xV2XkUkX12'};
        var user3 = { id:3, name:'Cookie3',session:'25j_7Sl6xDq2Kc3ym0fmrSSk2xV2XkUkX13'};
        this.$cookies.set('user',user);
        this.$cookies.set('user2',user2);
        this.$cookies.set('user3',user3);
        // print user name

        console.log("Cookie Sesstion:", this.$cookies.get('user').session);
        console.log("Cookie Name:", this.$cookies.get('user').name);
        console.log("Cookie Name:", this.$cookies.get('user2').name);
        console.log("Cookie Name:", this.$cookies.get('user3').name);
        // $cookies.remove('user');
        // console.log("Cookie Name:", this.$cookies.get('user').name);
        
        // Displaying all cookies:
        console.log(this.$cookies.keys());

    },
    methods: {
        readData: function() {
            axios.get("https://jsonplaceholder.typicode.com/posts").then(response => (this.weatherDataList = response.data));
            // fetch("https://jsonplaceholder.typicode.com/posts")
            //     .then(response => response.json())
            //     .then(data => (this.weatherDataList = data));
        },
        updateButton: function(utitle,ubody) {
            this.title = utitle;
            this.temp_title = utitle;
            this.body = ubody;
        },
        updateData: function() {
            this.errors = [];

            if (!this.title) {
                this.errors.push("Title required.");
            }
            if (!this.body) {
                this.errors.push('Body required.');
            }

            if(this.title && this.body){
                for (var i=0 ; i < this.weatherDataList.length ; i++)
                {
                    if (this.weatherDataList[i].title == this.temp_title ) {
                        this.weatherDataList[i].title = this.title;
                        this.weatherDataList[i].body = this.body;
                    }
                }
            }
        },
        deleteData: function(title) {
            for (var i=0 ; i < this.weatherDataList.length ; i++)
            {
                if (this.weatherDataList[i].title == title ) {
                    this.weatherDataList.splice(i,1);
                }
            }

        },

        createData: function() {
            this.errors = [];

            if (!this.title) {
                this.errors.push("Title required.");
            }
            if (!this.body) {
                this.errors.push('Body required.');
            }

            if(this.title && this.body){
                var last_element= this.weatherDataList.length;
                this.weatherDataList.push({userid:1, id: last_element+1, title: this.title, body: this.body });
            }
        }
        
    }
    });
</script>
